# demo-npm-git-semver

Demo of using git+semver as npm dependencies.

**This is part of a [blog post](https://davistobias.com/articles/semver-ranges-for-git-hosted-npm-modules/).**

---

This module isn't published on npm, so you have
to use a git reference to install it, e.g.:

```bash
npm install --save git+ssh://git@bitbucket.org/saibotsivad/demo-npm-git-semver.git
```

Then you can make sure you've installed it:

```js
const demo = require('demo-npm-git-semver')
console.log(demo())
```

Of course, the problem here is that you don't know what
version you installed, because you didn't specify a git
commit hash or version or anything.

One solution is to use the commit hash:

```bash
npm install --save git+ssh://git@bitbucket.org/saibotsivad/demo-npm-git-semver.git#1cbcec73
```

But what we really want is to be able to use proper
[semver ranges](https://docs.npmjs.com/getting-started/semantic-versioning)
(see also this handy [semver calculator](https://semver.npmjs.com/)),
that way if a patch is published we can get it automatically.

Conveniently, npm allows you to specify a semver range instead
of a hash!

Instead of `#1cbcec73` you use, for example, `#semver:^1.0.3`.

For example, if you wanted to say: use _at least_ version 1.0.3
but use the latest 1.x.y:

```bash
npm install --save git+ssh://git@bitbucket.org/saibotsivad/demo-npm-git-semver.git#semver:^1.0.3
```

In that demo repository, the highest 1.x version that I created
is 1.1.3, so that's what will be installed:

```js
const demo = require('demo-npm-git-semver')
console.log(demo()); // => 1.1.3
```
